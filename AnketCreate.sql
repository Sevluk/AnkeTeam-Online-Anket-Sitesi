
CREATE TABLE HesapTürü(
	HesapId int primary key,
	HesapTip varchar(40)
);
CREATE TABLE KurumsalKullanici(
	KurumId int primary key,
	KurumAd varchar(40),
	KullaniciSayisi int NOT NULL,
	Ulke varchar(40),
	KullaniciAd varchar(40)
	
);
CREATE TABLE Kullanici(
	KullaniciAd varchar(40) primary key,
	Mail varchar(40) NOT NULL,
	parola varchar(40) NOT NULL,
	HesapId int,
	AnketId int,
	FaturaNo int,
	KurumId int,
	foreign key(HesapId) references HesapTürü(HesapId),
	foreign key(KurumId) references KurumsalKullanici(KurumId)
	
);

CREATE TABLE Sablon(
	SablonId int primary key,
	SablonAd varchar(100),
	HesapId int,
	foreign key(HesapId) references HesapTürü(HesapId)
);
CREATE TABLE Sablonİcindekiler(
	İcindekilerId int primary key,
	S_Adi varchar(100),
	SoruSayisi int,
	SablonId int,
	foreign key(SablonId) references Sablon(SablonId)
);
CREATE TABLE SablonSoru(
	SablonSoruId int primary key,
	SablonSoruText varchar(200),
	İcindekilerId int,
	foreign key(İcindekilerId) references Sablonİcindekiler(İcindekilerId)
);
CREATE TABLE SablonCevap(
	SablonCevapId int primary key,
	SablonCevapText varchar(100),
	SablonSoruId int,
	foreign key(SablonSoruId) references SablonSoru(SablonSoruId)
);


CREATE TABLE Tema(
	TemaId int primary key,
	TemaAd varchar(40)
);
CREATE TABLE Anket(
	AnketId int primary key,
	AnketBaslik varchar(50),
	SayfaBaslik varchar(50),
	A_OlusturmaTarih date,
	A_DegistirmeTarih date,
	TemaId int,
	AnketDiliId int,
	AnketSecimId int,
	KullaniciAd varchar(40),
	SablonId int,
	foreign key(TemaId) references Tema(TemaId),
	foreign key(KullaniciAd) references Kullanici(KullaniciAd),
	foreign key(SablonId) references Sablon(SablonId)
);
CREATE TABLE SoruTip(
	SoruTipKod int primary key,
	SoruTipAd varchar(40)
);
CREATE TABLE Sorular(
	SoruId int primary key,
	SoruText varchar(200),
	SoruNumarasi int,
	AnketId int,
	SoruTipKod int,
	foreign key(AnketId) references Anket(AnketId),
	foreign key(SoruTipKod) references SoruTip(SoruTipKod)
);
CREATE TABLE Cevaplar(
	CevapId int primary key,
	CevapText varchar(100),
	
);
CREATE TABLE CoktanSecmeli(
	SoruId int,
	CevapId int,
    foreign key(SoruId) references Sorular(SoruId),
    foreign key(CevapId) references Cevaplar(CevapId)
);
CREATE TABLE EvetHayir(
	SoruId int,
	CevapId int,
    foreign key(SoruId) references Sorular(SoruId),
    foreign key(CevapId) references Cevaplar(CevapId)
);
CREATE TABLE Derecelendirme(
	SoruId int,
	CevapId int,
    foreign key(SoruId) references Sorular(SoruId),
    foreign key(CevapId) references Cevaplar(CevapId)
);

CREATE TABLE Odeme(
	OdemeId int primary key,
	OdemeTip varchar(40)
);
CREATE TABLE Fatura(
	FaturaNo int primary key,
	FaturaTutar money,
	F_KesimTarihi date,
	OdemeId int,
	KullaniciAd varchar(40),
	foreign key(OdemeId) references Odeme(OdemeId),
	foreign key(KullaniciAd) references Kullanici(KullaniciAd)
);
CREATE TABLE Katilimci(
	KatilimciId int primary key
);
CREATE TABLE Katilim(
	KatilimciId int primary key,
	SoruId int,
	CevapId int,
	AnketId int,
	foreign key(SoruId) references Sorular(SoruId),
	foreign key(CevapId) references Cevaplar(CevapId),
	foreign key(AnketId) references Anket(AnketId),
	foreign key(KatilimciId) references Katilimci(KatilimciId)
);