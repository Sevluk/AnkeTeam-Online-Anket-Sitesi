﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult KullanimSekli()
        {
            ViewBag.Message = "kullanım sekli";
            return View();
        }
        public ActionResult Ornekler()
        {
            ViewBag.Message = "ornekler";
            return View();

        }
        public ActionResult AnketHizmetleri()
        {
            ViewBag.Message = "anket hizmetleri";
            return View();
        }
        public ActionResult PlanlarveFiyatlandirma()
        {
            ViewBag.Message = "planlar ve fiyatlandirma";
            return View();

        }
        public ActionResult AnketSablonlari()
        {
            ViewBag.Message = "anket sablonlari";
            return View();

        }
        public ActionResult AnketTurleri()
        {
            ViewBag.Message = "anket turleri";
            return View();

        }
        public ActionResult AnketIpuclari()
        {
            ViewBag.Message = "anket ipuclari";
            return View();

        }
        public ActionResult AnketKistaslari()
        {
            ViewBag.Message = "anket kistaslari";
            return View();

        }
    }
}