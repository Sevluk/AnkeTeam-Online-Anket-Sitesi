--(TemaId,TemaAd)
INSERT INTO Tema VALUES(1,'mavi');
INSERT INTO Tema VALUES(2,'ye�il');
INSERT INTO Tema VALUES(3,'gri');
INSERT INTO Tema VALUES(4,'mor');
INSERT INTO Tema VALUES(5,'bordo');


--(SoruTipKod,SoruTipAd)
INSERT INTO SoruTip VALUES(1,'�oktanSe�meli');
INSERT INTO SoruTip VALUES(2,'Evet/Hay�r');
INSERT INTO SoruTip VALUES(3,'Derecelendirme');


--(HesapId,HesapTip)
INSERT INTO HesapT�r� VALUES(1,'Basic');
INSERT INTO HesapT�r� VALUES(2,'Select');
INSERT INTO HesapT�r� VALUES(3,'Gold');
INSERT INTO HesapT�r� VALUES(4,'Platinum');


--(OdemeId,OdemeTip)
INSERT INTO Odeme VALUES(1,'Havale');
INSERT INTO Odeme VALUES(2,'KrediKart�');

select si.S_Adi,h.HesapTip,si.SoruSayisi
from Sablon s inner join Sablon�cindekiler si on s.SablonId=si.SablonId inner join 
HesapT�r� h on s.HesapId=h.HesapId
where s.SablonAd='E�itim Anketi �ablonu'

--(SablonId,SablonAd,HesapId) 
INSERT INTO Sablon VALUES(1,'E�itim Anketi �ablonu',1);
INSERT INTO Sablon VALUES(2,'E�itim Anketi �ablonu',2);
INSERT INTO Sablon VALUES(3,'�nsan Kaynaklar� Anketi �ablonu',3);
INSERT INTO Sablon VALUES(4,'�nsan Kaynaklar� Anketi �ablonu',1);
INSERT INTO Sablon VALUES(5,'Kar Amac� G�tmeyen Anketi �ablonu',1);
INSERT INTO Sablon VALUES(6,'M��teri Geri Bildirim Anketi �ablonu',2);
INSERT INTO Sablon VALUES(7,'M��teri Geri Bildirim Anketi �ablonu',1);

--(�cindekilerId,S_Adi,SoruSayisi,SablonId)
INSERT INTO Sablon�cindekiler VALUES(1,'Ders De�erlendirme Anketi �ablonu',10,1);
INSERT INTO Sablon�cindekiler VALUES(2,'�niversite ��renci Memnuniyet Anketi �ablonu',16,2);
INSERT INTO Sablon�cindekiler VALUES(3,'�al��an De�erlendirme Anketi �ablonu',17,3);
INSERT INTO Sablon�cindekiler VALUES(4,'��yeri Memnuniyeti Anketi �ablonu',10,4);
INSERT INTO Sablon�cindekiler VALUES(5,'Ba��� Etkinli�i Anketi �ablonu',10,5);
INSERT INTO Sablon�cindekiler VALUES(6,'M��teri Memnuniyeti Anketi �ablonu',12,6);
INSERT INTO Sablon�cindekiler VALUES(7,'Web Sitesi Tasar�m� Anketi �ablonu',7,7);

--(SablonSoruId,SablonSoruText,�cindekilerId)
INSERT INTO SablonSoru VALUES(1,'1. ��retmeninizin ��retiminden memnun musunuz, memnun de�il misiniz, yoksa n�tr m�s�n�z?',1);
INSERT INTO SablonSoru VALUES(2,'2. ��retmeniniz ders i�eri�iyle ilgili ne kadar bilgiliydi?',1);
INSERT INTO SablonSoru VALUES(3,'3. ��retmeninizin dersin hedefleriyle ilgili a��klamas� ne kadar netti?',1);
INSERT INTO SablonSoru VALUES(4,'4. ��retmeninizin zor konularla ilgili a��klamalar� ne kadar netti?',1);
INSERT INTO SablonSoru VALUES(5,'5. ��retmeninizin konular� anlat��� �ok mu h�zl�yd�, �ok mu yava�t� yoksa karar�nda m�yd�?',1);
INSERT INTO SablonSoru VALUES(6,'6. ��retmeniniz ��rencilerin ��renme seviyesiyle ne kadar ilgiliydi?',1);
INSERT INTO SablonSoru VALUES(7,'7. ��retmeninizin sizi ��renmeye te�vik etmesi ne kadar iyiydi?',1);
INSERT INTO SablonSoru VALUES(8,'8. S�n�f d���nda ��retmeninize ula�man�z ne kadar kolayd�?',1);
INSERT INTO SablonSoru VALUES(9,'9. Sizce ��retmeninizin size verdi�i not ne kadar adil?',1);
INSERT INTO SablonSoru VALUES(10,'10. Ders gere�leri ne kadar faydal�yd�?',1);

INSERT INTO SablonSoru VALUES(11,'1. Genel olarak bu �niversitedeki deneyiminizden memnun musunuz, memnun de�il misiniz, yoksa n�tr m�s�n�z?',2);
INSERT INTO SablonSoru VALUES(12,'2. Bu �niversitedeki ��retim g�revlilerinin e�itimi ne kadar iyi?',2);
INSERT INTO SablonSoru VALUES(13,'3. Bu �niversitedeki tesisler ne kadar iyi korunuyor?',2);
INSERT INTO SablonSoru VALUES(14,'4. Akademik dan��man�n�z ne kadar yard�msever?',2);
INSERT INTO SablonSoru VALUES(15,'5. Bu �niversitede derslere kaydolmak ne kadar kolay?',2);
INSERT INTO SablonSoru VALUES(16,'6. Kamp�ste kendinizi ne kadar g�vende hissediyorsunuz?',2);
INSERT INTO SablonSoru VALUES(17,'7. Kamp�steki sa�l�k merkezinin personeli ne kadar yard�msever?',2);
INSERT INTO SablonSoru VALUES(18,'8. Kamp�steki kariyer merkezinde verilen hizmetler ne kadar faydal�?',2);
INSERT INTO SablonSoru VALUES(19,'9. �niversitenin k�t�phane sisteminden gereken kaynaklara ula�man�z ne kadar kolay?',2);
INSERT INTO SablonSoru VALUES(20,'10. Bu �niversitedeki yurt tesisleri ne kadar kalabal�k?',2);
INSERT INTO SablonSoru VALUES(21,'11. Bu �niversitede sunulan yiyecekler ne kadar sa�l�kl�?',2);
INSERT INTO SablonSoru VALUES(22,'12. �niversitenin destekledi�i ders d��� etkinlik se�eneklerinden ne kadar memnunsunuz?',2);
INSERT INTO SablonSoru VALUES(23,'13. Bu �niversitedeki idareciler sorular�n�za ve endi�elerinize kar�� ne kadar duyarl�?',2);
INSERT INTO SablonSoru VALUES(24,'14. Bu �niversitedeki y�netim prosed�rleri ne kadar adil?',2);
INSERT INTO SablonSoru VALUES(25,'15. Gelecek y�l bu �niversiteye devam etmeniz ne kadar m�mk�n?',2);
INSERT INTO SablonSoru VALUES(26,'16. Bu �niversiteyi ba�kalar�na �nermeniz ne kadar m�mk�n?',2);


INSERT INTO SablonSoru VALUES(27,'1. Genel olarak �al��an�n�z i�inde ne kadar etkili?',3);
INSERT INTO SablonSoru VALUES(28,'2. �al��an�n�z�n davran��lar� ne kadar profesyonel?',3);
INSERT INTO SablonSoru VALUES(29,'3. �al��an�n�z kendisi ile ilgili belirlenen hedeflere ula�mak i�in ne kadar iyi �al���yor?',3);
INSERT INTO SablonSoru VALUES(30,'4. �al��an�n�z i�e ne s�kl�kta ge� kal�yor?',3);
INSERT INTO SablonSoru VALUES(31,'5. �al��an�n�z de�i�en �nceliklere ne kadar h�zl� adapte oluyor?',3);
INSERT INTO SablonSoru VALUES(32,'6. �al��an�n�z taleplere ne kadar h�zl� kar��l�k veriyor?',3);
INSERT INTO SablonSoru VALUES(33,'7. �al��an�n�z s�re k�s�tlamalar�na ne s�kl�kta uyuyor?',3);
INSERT INTO SablonSoru VALUES(34,'8. �al��an�n�z i�iyle ilgili ele�tirileri ne kadar iyi kar��l�yor?',3);
INSERT INTO SablonSoru VALUES(35,'9. �al��an�n�z di�er �al��anlarla ne kadar iyi i�birli�i yap�yor?',3);
INSERT INTO SablonSoru VALUES(36,'10. �al��an�n�z ne kadar �al��kan?',3);
INSERT INTO SablonSoru VALUES(37,'11. �al��an�n�z beklentilerinizi ne s�kl�kta a��yor?',3);
INSERT INTO SablonSoru VALUES(38,'12. �al��an�n�z beklentilerinizi kar��lamakta ne s�kl�kta ba�ar�s�z oluyor?',3);
INSERT INTO SablonSoru VALUES(39,'13. �al��an�n�z kendi hatalar�n� kabul etmeye ne kadar istekli?',3);
INSERT INTO SablonSoru VALUES(40,'14. �al��an�n�z ne s�kl�kta kendi hatalar�n�n sorumlulu�unu al�yor?',3);
INSERT INTO SablonSoru VALUES(41,'15. �al��an�n�z�n do�ru kararlar� verebilece�ine ne kadar inan�yorsunuz?',3);
INSERT INTO SablonSoru VALUES(42,'16. �al��an�n�z ayr�nt�lara ne kadar dikkat ediyor?',3);
INSERT INTO SablonSoru VALUES(43,'17. �al��an�n�z�n size kar�� davran��lar� ne kadar sayg�l�?',3);



INSERT INTO SablonSoru VALUES(44,'1. Genel olarak i�yerinizle ilgili hisleriniz olumlu mu, olumsuz mu yoksa n�tr m�?',4);
INSERT INTO SablonSoru VALUES(45,'2. Genel olarak i�inizden memnun musunuz, memnun de�il misiniz yoksa n�tr m�s�n�z?',4);
INSERT INTO SablonSoru VALUES(46,'3. ��vereninizin �al��ma ortam� olumlu mu, olumsuz mu yoksa n�tr m�?',4);
INSERT INTO SablonSoru VALUES(47,'4. �irketinizde �al��anlar�n yetenekleri ne kadar iyi kullan�l�yor?',4);
INSERT INTO SablonSoru VALUES(48,'5. �al��anlar �irket hedeflerinin belirlenmesine ne kadar kat�l�yor?',4);
INSERT INTO SablonSoru VALUES(49,'6. ��vereninizin �irkete ili�kin temel de�erlerine inanc�n�z ne kadar g��l�?',4);
INSERT INTO SablonSoru VALUES(50,'7. �irket hedefleri ne kadar net?',4);
INSERT INTO SablonSoru VALUES(51,'8. �irket hedefleri ne kadar ger�ek�i?',4);
INSERT INTO SablonSoru VALUES(52,'9. ��vereninizin markas�yla ne kadar gurur duyuyorsunuz?',4);
INSERT INTO SablonSoru VALUES(53,'10. Sizce bu �irket �r�n kalitesine ne kadar de�er veriyor?',4);


INSERT INTO SablonSoru VALUES(54,'1. Genel olarak ba��� etkinli�imizdeki deneyiminizden memnun kald�n�z m�, memnun kalmad�n�z m�, yoksa n�tr m� kald�n�z?',5);
INSERT INTO SablonSoru VALUES(55,'2. Kurumumuzun katk�n�z�n nas�l harcanaca��na dair a��klamalar� ne kadar netti?',5);
INSERT INTO SablonSoru VALUES(56,'3. Kurumumuzun ba��� toplama ama�lar�na dair a��klamalar� ne kadar netti?',5);
INSERT INTO SablonSoru VALUES(57,'4. Gelecek y�l ba��� toplama etkinli�imize kat�lman�z ne kadar m�mk�n?',5);
INSERT INTO SablonSoru VALUES(58,'5. Ba��� etkinli�imize kat�lman�z�n maliyeti �ok mu y�ksekti, �ok mu d���kt�, yoksa karar�nda m�yd�?',5);
INSERT INTO SablonSoru VALUES(59,'6. Bir hay�r kurumuna veya kar amac� g�tmeyen bir kurulu�a ne s�kl�kta ba��� yap�yorsunuz?',5);
INSERT INTO SablonSoru VALUES(60,'7. En fazla ba��� yapt���n�z hay�r kurumuna veya kar amac� g�tmeyen kurulu�a bir ba��� daha yapman�z ne kadar m�mk�n?',5);
INSERT INTO SablonSoru VALUES(61,'8. Son ba��� yapt���n�z hay�r kurumuna veya kar amac� g�tmeyen kurulu�a bir ba��� daha yapman�z ne kadar m�mk�n?',5);
INSERT INTO SablonSoru VALUES(62,'9. �n�m�zdeki 12 ay i�inde bir hay�r kurumuna veya kar amac� g�tmeyen bir kurulu�a ba��� yapman�z ne kadar m�mk�n?',5);
INSERT INTO SablonSoru VALUES(63,'10. Hay�r kurumlar�n�n veya kar amac� g�tmeyen kurulu�lar�n �o�unun y�netimi sizce ne kadar iyi?',5);


INSERT INTO SablonSoru VALUES(64,'1. Genel olarak hizmetimizden memnun musunuz, memnun de�il misiniz yoksa n�tr m�s�n�z?',6);
INSERT INTO SablonSoru VALUES(65,'2. Ge�mi�te hizmetimizi ne s�kl�kta kulland�n�z?',6);
INSERT INTO SablonSoru VALUES(66,'3. Hizmetimizi kulland�ktan sonra, sizce fiyatlar�m�z �ok mu y�ksek, �ok mu d���k yoksa karar�nda m�?',6);
INSERT INTO SablonSoru VALUES(67,'4. Rakiplerimize nazaran fiyatlar�m�z daha m� y�ksek, daha m� d���k yoksa hemen hemen ayn� m�?',6);
INSERT INTO SablonSoru VALUES(68,'5. Rakiplerimize nazaran hizmet kalitemiz daha m� iyi, daha m� k�t� yoksa hemen hemen ayn� m�?',6);
INSERT INTO SablonSoru VALUES(69,'6. Size g�re hizmetimiz ne kadar faydal�?',6);
INSERT INTO SablonSoru VALUES(70,'7. Size g�re �irketimiz ihtiya�lar�n�z� ne kadar anl�yor?',6);
INSERT INTO SablonSoru VALUES(71,'8. Size g�re �irketimiz gizlili�inizi ne kadar koruyor?',6);
INSERT INTO SablonSoru VALUES(72,'9. Genel olarak �irketimiz hizmetimizle ilgili sorular�n�za veya endi�elerinize kar�� ne kadar hassas?',6);
INSERT INTO SablonSoru VALUES(73,'10. �leride hizmetimizi kullanman�z ne kadar m�mk�n?',6);
INSERT INTO SablonSoru VALUES(74,'11. Ba�kalar�n� hizmetimizi kullanmaktan vazge�irmeniz ne kadar m�mk�n?',6);
INSERT INTO SablonSoru VALUES(75,'12. Hizmetimizi ba�kalar�na �nermeniz ne kadar m�mk�n?',6);



INSERT INTO SablonSoru VALUES(76,'1. Genel olarak web sitemizden memnun musunuz, memnun de�il misiniz yoksa n�tr m�s�n�z?',7);
INSERT INTO SablonSoru VALUES(77,'2. Web sitemizde �deme i�lemi ne kadar kolay?',7);
INSERT INTO SablonSoru VALUES(78,'3. Web sitemizin �evrimi�i �deme i�leminin g�venli�ine ne kadar inan�yorsunuz?',7);
INSERT INTO SablonSoru VALUES(79,'4. Web sitemizin g�r�n�m� ve kullan�m� ne kadar profesyonel?',7);
INSERT INTO SablonSoru VALUES(80,'5. Web sitemiz g�rsel olarak ne kadar �ekici?',7);
INSERT INTO SablonSoru VALUES(81,'6. Web sitemizde bulunan bilgiler ne kadar net?',7);
INSERT INTO SablonSoru VALUES(82,'7. Web sitemizde arad���n�z bilgileri bulman�z ne kadar kolay?',7);


--(SablonCevapId,SablonCevapText,SablonSoruId)

--�ablon 1
INSERT INTO SablonCevap VALUES(1,'�ok memnunum',1);
INSERT INTO SablonCevap VALUES(2,'K�smen memnunum',1);
INSERT INTO SablonCevap VALUES(3,'Pek memnun de�ilim',1);
INSERT INTO SablonCevap VALUES(4,'Hi� memnun de�ilim',1);
INSERT INTO SablonCevap VALUES(5,'�ok bilgiliydi',2);
INSERT INTO SablonCevap VALUES(6,'Orta derecede bilgiliydi',2);
INSERT INTO SablonCevap VALUES(7,'Biraz bilgiliydi',2);
INSERT INTO SablonCevap VALUES(8,'Hi� bilgili de�ildi',2);
INSERT INTO SablonCevap VALUES(9,'�ok netti',3);
INSERT INTO SablonCevap VALUES(10,'Orta derecede netti',3);
INSERT INTO SablonCevap VALUES(11,'Biraz netti',3);
INSERT INTO SablonCevap VALUES(12,'Hi� net de�ildi',3);
INSERT INTO SablonCevap VALUES(13,'�ok netti',4);
INSERT INTO SablonCevap VALUES(14,'Orta derecede netti',4);
INSERT INTO SablonCevap VALUES(15,'Biraz netti',4);
INSERT INTO SablonCevap VALUES(16,'Hi� net de�ildi',4);
INSERT INTO SablonCevap VALUES(17,'Olduk�a h�zl�yd�',5);
INSERT INTO SablonCevap VALUES(18,'Biraz h�zl�yd�',5);
INSERT INTO SablonCevap VALUES(19,'Biraz yava�t�',5);
INSERT INTO SablonCevap VALUES(20,'�ok �ok yava�t�',5);
INSERT INTO SablonCevap VALUES(21,'�ok ilgiliydi',6);
INSERT INTO SablonCevap VALUES(22,'Orta derecede ilgiliydi',6);
INSERT INTO SablonCevap VALUES(23,'Biraz ilgiliydi',6);
INSERT INTO SablonCevap VALUES(24,'Hi� ilgili de�ildi',6);
INSERT INTO SablonCevap VALUES(25,'�ok iyiydi',7);
INSERT INTO SablonCevap VALUES(26,'Orta derecede iyiydi',7);
INSERT INTO SablonCevap VALUES(27,'K�smen iyiydi',7);
INSERT INTO SablonCevap VALUES(28,'Hi� iyi de�ildi',7);
INSERT INTO SablonCevap VALUES(29,'�ok kolayd�',8);
INSERT INTO SablonCevap VALUES(30,'Orta derecede kolayd�',8);
INSERT INTO SablonCevap VALUES(31,'Biraz kolayd�',8);
INSERT INTO SablonCevap VALUES(32,'Hi� kolay de�ildi',8);
INSERT INTO SablonCevap VALUES(33,'�ok adil',9);
INSERT INTO SablonCevap VALUES(34,'Orta derecede adil',9);
INSERT INTO SablonCevap VALUES(35,'K�smen adil',9);
INSERT INTO SablonCevap VALUES(36,'Hi� adil de�il',9);
INSERT INTO SablonCevap VALUES(37,'�ok faydal�yd�',10);
INSERT INTO SablonCevap VALUES(38,'Orta derecede faydal�yd�',10);
INSERT INTO SablonCevap VALUES(39,'K�smen faydal�yd�',10);
INSERT INTO SablonCevap VALUES(40,'Hi� faydal� de�ildi',10);


--�ablon 2
INSERT INTO SablonCevap VALUES(41,'�ok memnunum',11);
INSERT INTO SablonCevap VALUES(42,'K�smen memnunum',11);
INSERT INTO SablonCevap VALUES(43,'Memnun de�ilim',11);
INSERT INTO SablonCevap VALUES(44,'Hi� memnun de�ilim',11);
INSERT INTO SablonCevap VALUES(45,'�ok iyi',12);
INSERT INTO SablonCevap VALUES(46,'Orta derecede iyi',12);
INSERT INTO SablonCevap VALUES(47,'K�smen iyi',12);
INSERT INTO SablonCevap VALUES(48,'Hi� iyi de�il',12);
INSERT INTO SablonCevap VALUES(49,'�ok iyi korunuyor',13);
INSERT INTO SablonCevap VALUES(50,'Orta derecede iyi korunuyor',13);
INSERT INTO SablonCevap VALUES(51,'K�smen iyi korunuyor',13);
INSERT INTO SablonCevap VALUES(52,'Hi� iyi korunmuyor',13);
INSERT INTO SablonCevap VALUES(53,'�ok yard�msever',14);
INSERT INTO SablonCevap VALUES(54,'Orta derecede yard�msever',14);
INSERT INTO SablonCevap VALUES(55,'K�smen yard�msever',14);
INSERT INTO SablonCevap VALUES(56,'Hi� yard�msever de�il',14);
INSERT INTO SablonCevap VALUES(57,'�ok kolay',15);
INSERT INTO SablonCevap VALUES(58,'Orta derecede kolay',15);
INSERT INTO SablonCevap VALUES(59,'Biraz kolay',15);
INSERT INTO SablonCevap VALUES(60,'Hi� kolay de�il',15);
INSERT INTO SablonCevap VALUES(61,'�ok g�vende hissediyorum',16);
INSERT INTO SablonCevap VALUES(62,'Orta derecede g�vende hissediyorum',16);
INSERT INTO SablonCevap VALUES(63,'K�smen g�vende hissediyorum',16);
INSERT INTO SablonCevap VALUES(64,'Hi� g�vende hissetmiyorum',16);
INSERT INTO SablonCevap VALUES(65,'�ok yard�msever',17);
INSERT INTO SablonCevap VALUES(66,'Orta derecede yard�msever',17);
INSERT INTO SablonCevap VALUES(67,'K�smen yard�msever',17);
INSERT INTO SablonCevap VALUES(68,'Hi� yard�msever de�il',17);
INSERT INTO SablonCevap VALUES(69,'�ok faydal�',18);
INSERT INTO SablonCevap VALUES(70,'Orta derecede faydal�',18);
INSERT INTO SablonCevap VALUES(71,'Biraz faydal�',18);
INSERT INTO SablonCevap VALUES(72,'Hi� faydal� de�il',18);
INSERT INTO SablonCevap VALUES(73,'�ok kolay',19);
INSERT INTO SablonCevap VALUES(74,'Orta derecede kolay',19);
INSERT INTO SablonCevap VALUES(75,'Biraz kolay',19);
INSERT INTO SablonCevap VALUES(76,'Hi� kolay de�il',19);
INSERT INTO SablonCevap VALUES(77,'�ok kalabal�k',20);
INSERT INTO SablonCevap VALUES(78,'Orta derecede kalabal�k',20);
INSERT INTO SablonCevap VALUES(79,'Biraz kalabal�k',20);
INSERT INTO SablonCevap VALUES(80,'Hi� kalabal�k de�il',20);
INSERT INTO SablonCevap VALUES(81,'�ok sa�l�kl�',21);
INSERT INTO SablonCevap VALUES(82,'Orta derecede sa�l�kl�',21);
INSERT INTO SablonCevap VALUES(83,'Biraz sa�l�kl�',21);
INSERT INTO SablonCevap VALUES(84,'Hi� sa�l�kl� de�il',21);
INSERT INTO SablonCevap VALUES(85,'�ok memnunum',22);
INSERT INTO SablonCevap VALUES(86,'Orta derecede memnunum',22);
INSERT INTO SablonCevap VALUES(87,'Biraz memnunum',22);
INSERT INTO SablonCevap VALUES(88,'Hi� memnun de�ilim',22);
INSERT INTO SablonCevap VALUES(89,'�ok duyarl�',23);
INSERT INTO SablonCevap VALUES(90,'Orta derecede duyarl�',23);
INSERT INTO SablonCevap VALUES(91,'K�smen duyarl�',23);
INSERT INTO SablonCevap VALUES(92,'Hi� duyarl� de�il',23);
INSERT INTO SablonCevap VALUES(93,'Olduk�a adil',24);
INSERT INTO SablonCevap VALUES(94,'Orta derecede adil',24);
INSERT INTO SablonCevap VALUES(95,'K�smen adil',24);
INSERT INTO SablonCevap VALUES(96,'Hi� adil de�il',24);
INSERT INTO SablonCevap VALUES(97,'�ok m�mk�n',25);
INSERT INTO SablonCevap VALUES(98,'K�smen m�mk�n',25);
INSERT INTO SablonCevap VALUES(99,'Hi� m�mk�n de�il',25);
INSERT INTO SablonCevap VALUES(100,'Benim i�in ge�erli de�il',25);
INSERT INTO SablonCevap VALUES(101,'�ok m�mk�n',26);
INSERT INTO SablonCevap VALUES(102,'Orta derecede m�mk�n',26);
INSERT INTO SablonCevap VALUES(103,'K�smen m�mk�n',26);
INSERT INTO SablonCevap VALUES(104,'Hi� m�mk�n de�il',26);

--�ablon 3 

INSERT INTO SablonCevap VALUES(105,'�ok etkili',27);
INSERT INTO SablonCevap VALUES(106,'Orta derecede etkili',27);
INSERT INTO SablonCevap VALUES(107,'Biraz etkili',27);
INSERT INTO SablonCevap VALUES(108,'Hi� etkili de�il',27);
INSERT INTO SablonCevap VALUES(109,'Olduk�a profesyonel',28);
INSERT INTO SablonCevap VALUES(110,'Orta derecede profesyonel',28);
INSERT INTO SablonCevap VALUES(111,'K�smen profesyonel',28);
INSERT INTO SablonCevap VALUES(112,'Hi� profesyonel de�il',28);
INSERT INTO SablonCevap VALUES(113,'�ok iyi',29);
INSERT INTO SablonCevap VALUES(114,'Orta derecede iyi',29);
INSERT INTO SablonCevap VALUES(115,'K�smen iyi',29);
INSERT INTO SablonCevap VALUES(116,'Hi� iyi de�il',29);
INSERT INTO SablonCevap VALUES(117,'�o�u zaman',30);
INSERT INTO SablonCevap VALUES(118,'Bazen',30);
INSERT INTO SablonCevap VALUES(119,'Arada bir',30);
INSERT INTO SablonCevap VALUES(120,'Hi�bir zaman',30);
INSERT INTO SablonCevap VALUES(121,'�ok h�zl�',31);
INSERT INTO SablonCevap VALUES(122,'Orta derecede h�zl�',31);
INSERT INTO SablonCevap VALUES(123,'Biraz h�zl�',31);
INSERT INTO SablonCevap VALUES(124,'Hi� h�zl� de�il',31);
INSERT INTO SablonCevap VALUES(125,'�ok h�zl�',32);
INSERT INTO SablonCevap VALUES(126,'Orta derecede h�zl�',32);
INSERT INTO SablonCevap VALUES(127,'Biraz h�zl�',32);
INSERT INTO SablonCevap VALUES(128,'Hi� h�zl� de�il',32);
INSERT INTO SablonCevap VALUES(129,'�o�u zaman',33);
INSERT INTO SablonCevap VALUES(130,'Bazen',33);
INSERT INTO SablonCevap VALUES(131,'Arada bir',33);
INSERT INTO SablonCevap VALUES(132,'Hi�bir zaman',33);
INSERT INTO SablonCevap VALUES(133,'�ok iyi',34);
INSERT INTO SablonCevap VALUES(134,'Orta derecede iyi',34);
INSERT INTO SablonCevap VALUES(135,'K�smen iyi',34);
INSERT INTO SablonCevap VALUES(136,'Hi� iyi de�il',34);
INSERT INTO SablonCevap VALUES(137,'�ok iyi',35);
INSERT INTO SablonCevap VALUES(138,'Orta derecede iyi',35);
INSERT INTO SablonCevap VALUES(139,'K�smen iyi',35);
INSERT INTO SablonCevap VALUES(140,'Hi� iyi de�il',35);
INSERT INTO SablonCevap VALUES(141,'�ok �al��kan',36);
INSERT INTO SablonCevap VALUES(142,'Orta derecede �al��kan',36);
INSERT INTO SablonCevap VALUES(143,'Biraz �al��kan',36);
INSERT INTO SablonCevap VALUES(144,'Hi� �al��kan de�il',36);
INSERT INTO SablonCevap VALUES(145,'�o�u zaman',37);
INSERT INTO SablonCevap VALUES(146,'Bazen',37);
INSERT INTO SablonCevap VALUES(147,'Arada bir',37);
INSERT INTO SablonCevap VALUES(148,'Hi�bir zaman',37);
INSERT INTO SablonCevap VALUES(149,'�o�u zaman',38);
INSERT INTO SablonCevap VALUES(150,'Bazen',38);
INSERT INTO SablonCevap VALUES(151,'Arada bir',38);
INSERT INTO SablonCevap VALUES(152,'Hi�bir zaman',38);
INSERT INTO SablonCevap VALUES(153,'�ok istekli',39);
INSERT INTO SablonCevap VALUES(154,'Orta derecede istekli',39);
INSERT INTO SablonCevap VALUES(155,'Biraz istekli',39);
INSERT INTO SablonCevap VALUES(156,'Hi� istekli de�il',39);
INSERT INTO SablonCevap VALUES(157,'�o�u zaman',40);
INSERT INTO SablonCevap VALUES(158,'Bazen',40);
INSERT INTO SablonCevap VALUES(159,'Arada bir',40);
INSERT INTO SablonCevap VALUES(160,'Hi�bir zaman',40);
INSERT INTO SablonCevap VALUES(161,'�ok inan�yorum',41);
INSERT INTO SablonCevap VALUES(162,'Orta derecede inan�yorum',41);
INSERT INTO SablonCevap VALUES(163,'K�smen inan�yorum',41);
INSERT INTO SablonCevap VALUES(164,'Hi� inanm�yorum',41);
INSERT INTO SablonCevap VALUES(165,'Son derece fazla dikkat ediyor',42);
INSERT INTO SablonCevap VALUES(166,'Orta derecede dikkat ediyor',42);
INSERT INTO SablonCevap VALUES(167,'Biraz dikkat ediyor',42);
INSERT INTO SablonCevap VALUES(168,'Hi� dikkat etmiyor',42);
INSERT INTO SablonCevap VALUES(169,'�ok sayg�l�',43);
INSERT INTO SablonCevap VALUES(170,'Orta derecede sayg�l�',43);
INSERT INTO SablonCevap VALUES(171,'Biraz sayg�l�',43);
INSERT INTO SablonCevap VALUES(172,'Hi� sayg�l� de�il',43);


--�ablon 4 

INSERT INTO SablonCevap VALUES(173,'Olduk�a olumlu',44);
INSERT INTO SablonCevap VALUES(174,'Biraz olumlu',44);
INSERT INTO SablonCevap VALUES(175,'Biraz olumsuz',44);
INSERT INTO SablonCevap VALUES(176,'Son derece olumsuz',44);
INSERT INTO SablonCevap VALUES(177,'�ok memnunum',45);
INSERT INTO SablonCevap VALUES(178,'K�smen memnunum',45);
INSERT INTO SablonCevap VALUES(179,'Pek memnun de�ilim',45);
INSERT INTO SablonCevap VALUES(180,'Hi� memnun de�ilim',45);
INSERT INTO SablonCevap VALUES(181,'Son derece olumlu',46);
INSERT INTO SablonCevap VALUES(182,'K�smen olumlu',46);
INSERT INTO SablonCevap VALUES(183,'Orta derecede olumsuz',46);
INSERT INTO SablonCevap VALUES(184,'Son derece olumsuz',46);
INSERT INTO SablonCevap VALUES(185,'�ok iyi',47);
INSERT INTO SablonCevap VALUES(186,'Orta derecede iyi',47);
INSERT INTO SablonCevap VALUES(187,'K�smen iyi',47);
INSERT INTO SablonCevap VALUES(188,'Hi� iyi de�il',47);
INSERT INTO SablonCevap VALUES(189,'�ok kat�l�yor',48);
INSERT INTO SablonCevap VALUES(190,'Orta derecede kat�l�yor',48);
INSERT INTO SablonCevap VALUES(191,'Biraz kat�l�yor',48);
INSERT INTO SablonCevap VALUES(192,'Hi� kat�lm�yor',48);
INSERT INTO SablonCevap VALUES(193,'Olduk�a g��l�',49);
INSERT INTO SablonCevap VALUES(194,'Orta derecede g��l�',49);
INSERT INTO SablonCevap VALUES(195,'K�smen g��l�',49);
INSERT INTO SablonCevap VALUES(196,'Hi� g��l� de�il',49);
INSERT INTO SablonCevap VALUES(197,'�ok net',50);
INSERT INTO SablonCevap VALUES(198,'Orta derecede net',50);
INSERT INTO SablonCevap VALUES(199,'K�smen net',50);
INSERT INTO SablonCevap VALUES(200,'Hi� net de�il',50);
INSERT INTO SablonCevap VALUES(201,'�ok ger�ek�i',51);
INSERT INTO SablonCevap VALUES(202,'Orta derecede ger�ek�i',51);
INSERT INTO SablonCevap VALUES(203,'K�smen ger�ek�i',51);
INSERT INTO SablonCevap VALUES(204,'Hi� ger�ek�i de�il',51);
INSERT INTO SablonCevap VALUES(205,'�ok gurur duyuyorum',52);
INSERT INTO SablonCevap VALUES(206,'Orta derecede gurur duyuyorum',52);
INSERT INTO SablonCevap VALUES(207,'Biraz gurur duyuyorum',52);
INSERT INTO SablonCevap VALUES(208,'Hi� gurur duymuyorum',52);
INSERT INTO SablonCevap VALUES(209,'Olduk�a',53);
INSERT INTO SablonCevap VALUES(210,'Orta derecede',53);
INSERT INTO SablonCevap VALUES(211,'Biraz',53);
INSERT INTO SablonCevap VALUES(212,'Hi� de�er vermiyor',53);

--�ablon 5
INSERT INTO SablonCevap VALUES(213,'Son derece memnun kald�m',54);
INSERT INTO SablonCevap VALUES(214,'K�smen memnunum kald�m',54);
INSERT INTO SablonCevap VALUES(215,'Orta derecede memnun kalmad�m',54);
INSERT INTO SablonCevap VALUES(216,'Hi� memnun kalmad�m',54);
INSERT INTO SablonCevap VALUES(217,'�ok netti',55);
INSERT INTO SablonCevap VALUES(218,'Orta derecede netti',55);
INSERT INTO SablonCevap VALUES(219,'Biraz netti',55);
INSERT INTO SablonCevap VALUES(220,'Hi� net de�ildi',55);
INSERT INTO SablonCevap VALUES(221,'�ok netti',56);
INSERT INTO SablonCevap VALUES(222,'Orta derecede netti',56);
INSERT INTO SablonCevap VALUES(223,'Biraz netti',56);
INSERT INTO SablonCevap VALUES(224,'Hi� net de�ildi',56);
INSERT INTO SablonCevap VALUES(225,'�ok m�mk�n',57);
INSERT INTO SablonCevap VALUES(226,'Orta derecede m�mk�n',57);
INSERT INTO SablonCevap VALUES(227,'K�smen m�mk�n',57);
INSERT INTO SablonCevap VALUES(228,'Hi� m�mk�n de�il',57);
INSERT INTO SablonCevap VALUES(229,'Olduk�a y�ksekti',58);
INSERT INTO SablonCevap VALUES(230,'Karar�ndayd�',58);
INSERT INTO SablonCevap VALUES(231,'Olduk�a d���kt�',58);
INSERT INTO SablonCevap VALUES(232,'�ok �ok d���kt�',58);
INSERT INTO SablonCevap VALUES(233,'�ok s�k',59);
INSERT INTO SablonCevap VALUES(234,'Orta s�kl�kta',59);
INSERT INTO SablonCevap VALUES(235,'Bazen',59);
INSERT INTO SablonCevap VALUES(236,'Hi�',59);
INSERT INTO SablonCevap VALUES(237,'�ok m�mk�n',60);
INSERT INTO SablonCevap VALUES(238,'Orta derecede m�mk�n',60);
INSERT INTO SablonCevap VALUES(239,'K�smen m�mk�n',60);
INSERT INTO SablonCevap VALUES(240,'Hi� m�mk�n de�il',60);
INSERT INTO SablonCevap VALUES(241,'�ok m�mk�n',61);
INSERT INTO SablonCevap VALUES(242,'Orta derecede m�mk�n',61);
INSERT INTO SablonCevap VALUES(243,'K�smen m�mk�n',61);
INSERT INTO SablonCevap VALUES(244,'Hi� m�mk�n de�il',61);
INSERT INTO SablonCevap VALUES(245,'�ok m�mk�n',62);
INSERT INTO SablonCevap VALUES(246,'Orta derecede m�mk�n',62);
INSERT INTO SablonCevap VALUES(247,'K�smen m�mk�n',62);
INSERT INTO SablonCevap VALUES(248,'Hi� m�mk�n de�il',62);
INSERT INTO SablonCevap VALUES(249,'�ok iyi',63);
INSERT INTO SablonCevap VALUES(250,'Orta derecede iyi',63);
INSERT INTO SablonCevap VALUES(251,'K�smen iyi',63);
INSERT INTO SablonCevap VALUES(252,'Hi� iyi de�il',63);

--�ablon 6
INSERT INTO SablonCevap VALUES(253,'�ok memnunum',64);
INSERT INTO SablonCevap VALUES(254,'Ne memnunum ne de de�ilim',64);
INSERT INTO SablonCevap VALUES(255,'Pek memnun de�ilim',64);
INSERT INTO SablonCevap VALUES(256,'Hi� memnun de�ilim',64);
INSERT INTO SablonCevap VALUES(257,'�ok s�k',65);
INSERT INTO SablonCevap VALUES(258,'Orta s�kl�kta',65);
INSERT INTO SablonCevap VALUES(259,'Bazen',65);
INSERT INTO SablonCevap VALUES(260,'Hi�',65);
INSERT INTO SablonCevap VALUES(261,'Olduk�a y�ksek',66);
INSERT INTO SablonCevap VALUES(262,'Karar�nda',66);
INSERT INTO SablonCevap VALUES(263,'Biraz d���k',66);
INSERT INTO SablonCevap VALUES(264,'�ok �ok d���k',66);
INSERT INTO SablonCevap VALUES(265,'�ok daha y�ksek',67);
INSERT INTO SablonCevap VALUES(266,'Hemen hemen ayn�',67);
INSERT INTO SablonCevap VALUES(267,'�ok daha d���k',67);
INSERT INTO SablonCevap VALUES(268,'Bilmiyorum',67);
INSERT INTO SablonCevap VALUES(269,'�ok daha iyi',68);
INSERT INTO SablonCevap VALUES(270,'Hemen hemen ayn�',68);
INSERT INTO SablonCevap VALUES(271,'�ok daha k�t�',68);
INSERT INTO SablonCevap VALUES(272,'Bilmiyorum',68);
INSERT INTO SablonCevap VALUES(273,'�ok faydal�',69);
INSERT INTO SablonCevap VALUES(274,'Orta derecede faydal�',69);
INSERT INTO SablonCevap VALUES(275,'Biraz faydal�',69);
INSERT INTO SablonCevap VALUES(276,'Hi� faydal� de�il',69);
INSERT INTO SablonCevap VALUES(277,'�ok iyi',70);
INSERT INTO SablonCevap VALUES(278,'Orta derecede iyi',70);
INSERT INTO SablonCevap VALUES(279,'K�smen iyi',70);
INSERT INTO SablonCevap VALUES(280,'Hi� iyi de�il',70);
INSERT INTO SablonCevap VALUES(281,'�ok iyi',71);
INSERT INTO SablonCevap VALUES(282,'Orta derecede iyi',71);
INSERT INTO SablonCevap VALUES(283,'K�smen iyi',71);
INSERT INTO SablonCevap VALUES(284,'Hi� iyi de�il',71);
INSERT INTO SablonCevap VALUES(285,'�ok hassas',72);
INSERT INTO SablonCevap VALUES(286,'Orta derecede hassas',72);
INSERT INTO SablonCevap VALUES(287,'Hi� hassas de�il',72);
INSERT INTO SablonCevap VALUES(288,'Benim i�in ge�erli de�il',72);
INSERT INTO SablonCevap VALUES(289,'�ok m�mk�n',73);
INSERT INTO SablonCevap VALUES(290,'Orta derecede m�mk�n',73);
INSERT INTO SablonCevap VALUES(291,'K�smen m�mk�n',73);
INSERT INTO SablonCevap VALUES(292,'Hi� m�mk�n de�il',73);
INSERT INTO SablonCevap VALUES(293,'�ok m�mk�n',74);
INSERT INTO SablonCevap VALUES(294,'Orta derecede m�mk�n',74);
INSERT INTO SablonCevap VALUES(295,'K�smen m�mk�n',74);
INSERT INTO SablonCevap VALUES(296,'Hi� m�mk�n de�il',74);
INSERT INTO SablonCevap VALUES(297,'�ok m�mk�n',75);
INSERT INTO SablonCevap VALUES(298,'Orta derecede m�mk�n',75);
INSERT INTO SablonCevap VALUES(299,'K�smen m�mk�n',75);
INSERT INTO SablonCevap VALUES(300,'Hi� m�mk�n de�il',75);


--�ablon 7			
			
INSERT INTO SablonCevap VALUES(301,'�ok memnunum',76);
INSERT INTO SablonCevap VALUES(302,'K�smen memnunum',76);
INSERT INTO SablonCevap VALUES(303,'Pek memnun de�ilim',76);
INSERT INTO SablonCevap VALUES(304,'Hi� memnun de�ilim',76);
INSERT INTO SablonCevap VALUES(305,'Son derece kolay',77);
INSERT INTO SablonCevap VALUES(306,'Orta derecede kolay',77);
INSERT INTO SablonCevap VALUES(307,'Biraz kolay',77);
INSERT INTO SablonCevap VALUES(308,'Hi� kolay de�il',77);
INSERT INTO SablonCevap VALUES(309,'�ok inan�yorum',78);
INSERT INTO SablonCevap VALUES(310,'Orta derecede inan�yorum',78);
INSERT INTO SablonCevap VALUES(311,'Biraz inan�yorum',78);
INSERT INTO SablonCevap VALUES(312,'Hi� inanm�yorum',78);
INSERT INTO SablonCevap VALUES(313,'Olduk�a profesyonel',79);
INSERT INTO SablonCevap VALUES(314,'Orta derecede profesyonel',79);
INSERT INTO SablonCevap VALUES(315,'K�smen profesyonel',79);
INSERT INTO SablonCevap VALUES(316,'Hi� profesyonel de�il',79);
INSERT INTO SablonCevap VALUES(317,'Olduk�a �ekici',80);
INSERT INTO SablonCevap VALUES(318,'Orta derecede �ekici',80);
INSERT INTO SablonCevap VALUES(319,'K�smen �ekici',80);
INSERT INTO SablonCevap VALUES(320,'Hi� �ekici de�il',80);
INSERT INTO SablonCevap VALUES(321,'�ok net',81);
INSERT INTO SablonCevap VALUES(322,'Orta derecede net',81);
INSERT INTO SablonCevap VALUES(323,'K�smen net',81);
INSERT INTO SablonCevap VALUES(324,'Hi� net de�il',81);
INSERT INTO SablonCevap VALUES(325,'�ok kolay',82);
INSERT INTO SablonCevap VALUES(326,'Orta derecede kolay',82);
INSERT INTO SablonCevap VALUES(327,'Biraz kolay',82);
INSERT INTO SablonCevap VALUES(328,'Hi� kolay de�il',82);

select *
from SablonCevap

select s.SablonSoruText,c.SablonCevapText
from SablonSoru s inner join SablonCevap c on s.SablonSoruId=c.SablonSoruId
where s.SablonSoruId=79